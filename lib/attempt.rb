class Attempt
  def initialize cards
    @cards = cards
    @criterias = [:color, :texture, :number, :form]
  end

  def resolve
    is_set ? victory_string : "Hum...\n"
  end

  def is_set
    criterias_in_common = @criterias.map { |criteria| number_of_same(criteria) }
    possibilities.include? criterias_in_common.sort
  end

  private

  def number_of_same criteria
    @cards.map(&criteria).uniq.size
  end

  def possibilities
    [
      [1, 1, 1, 3],
      [1, 1, 3, 3],
      [1, 3, 3, 3],
      [3, 3, 3, 3]
    ]
  end

  def victory_string
    victory_colors = "\e[31m##\e[0m\e[32m##\e[0m\e[34m##\e[0m"
    Array.new(6, victory_colors)
    .insert(3, " Set ! ")
    .push("\n")
    .join
  end
end
