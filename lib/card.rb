class Card
  attr_reader :color, :texture, :form, :number

  def initialize(args)
    @color = args[0]
    @texture = args[1]
    @form = args[2]
    @number = args[3]
  end

  def display
    framed_content = display_content.map do |line|
      "│#{line}║"
    end
    [ "┌─────────────────╖" ] +
     framed_content +
    [ "│                 ║" ] +
    [ "╘═════════════════╝" ]
  end

  private

  def display_content
    empty_line = "                 "
    if @number == "3"
      [ empty_line ] +
      colored_shape +
      [ empty_line ] +
      colored_shape +
      [ empty_line ] +
      colored_shape
    elsif @number == "2"
      Array.new(3, empty_line) +
      colored_shape +
      Array.new(1, empty_line) +
      colored_shape +
      Array.new(2, empty_line)
    elsif @number == "1"
      Array.new(5, empty_line) +
      colored_shape +
      Array.new(4, empty_line)
    end
  end

  def colored_shape
    ascii_display.map do | shape |
      "\e[#{@color}m#{shape}\e[0m"
    end
  end

  def ascii_display
    if @texture == "empty"
      if @form == "~"
        [
          "     ╱▔▔╲▁╱▔╲    ",
          "    ╱       ╱    ",
          "    ╲▁╱▔╲▁▁╱     "
        ]
      elsif @form == "O"
        [
          "    ╱▔▔▔▔▔▔▔╲    ",
          "    ▏       ▕    ",
          "    ╲▁▁▁▁▁▁▁╱    "
        ]
      elsif @form == "#"
        [
          "   ▕▔▔▔▔▔▔▔▔▏    ",
          "   ▕        ▏    ",
          "   ▕▁▁▁▁▁▁▁▁▏    "
        ]
      end
    elsif @texture == "plain"
      if @form == "~"
        [
          "     d8b  d8     ",
          "    d888888P     ",
          "    8P  Y8P      "
        ]
      elsif @form == "O"
        [
          "     d88888b     ",
          "    888888888    ",
          "     Y888888     "
        ]
      elsif @form == "#"
        [
          "    88888888     ",
          "    88888888     ",
          "    88888888     "
        ]
      end
    elsif @texture == "line"
      if @form == "~"
        [
          "     ▟█▙  ▟▙     ",
          "    ▟██████▛     ",
          "    ▜▛  ▜█▛      "
        ]
      elsif @form == "O"
        [
          "     ▟█████▙     ",
          "    █████████    ",
          "     ▜█████▛     "
        ]
      elsif @form == "#"
        [
          "    ████████     ",
          "    ████████     ",
          "    ████████     "
        ]
      end
    end
  end
end
