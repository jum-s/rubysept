class Tapis
  attr_accessor :cards

  def initialize
    @cards = []
  end

  def add_cards deck
    3.times { add_a_card(deck) }
  end

  def deal deck
    12.times { add_a_card(deck) }
  end

  def position card
    @cards.rindex card
  end

  def display
    print "Cards on the table : \n"
    print formatted_tapis
    print "\n\n"
  end

  def letter card
    to_letter(position(card) + 1)
  end

  private

  def to_letter n
    ('a'..'p').to_a[n-1]
  end

  def formatted_tapis
    number_of_rows = (@cards.size / 4.0).round
    tapis = [(0..3), (4..7), (8..11)].map do |row_number|
      build_row(@cards[row_number])
    end
    tapis << build_row(@cards[12..15]) if @cards.size > 12
    tapis.join "\n"
  end

  def build_row cards
    row = []
    cards[0].display.each.with_index do | line, i|
      line = build_row_line(cards, i)
      row.push line.join(' ')
    end
    row.join "\n"
  end

  def build_row_line cards, i
    line = []
    if i == 1
      (0..cards.size-1).map do |j|
        card = cards[j]
        card_position = position(card) + 1
        letter = to_letter card_position
        original_display = card.display[i].chars
        lettered_diplay = replace_line_letter(original_display, letter)
        line.push lettered_diplay.join
      end
    else
      (0..cards.size-1).map do |j|
        line.push cards[j].display[i]
      end
    end
    line
  end

  def replace_line_letter original_display, letter
    original_display.delete_at(2)
    original_display.insert(1, letter)
  end

  def add_a_card deck
    new_card = deck.cards.shift
    @cards.push new_card
  end
end
