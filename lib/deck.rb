require_relative 'card'

COLOR = [ 31, 32, 34 ]
TEXTURE = [ 'plain', 'empty', 'line' ]
FORM = [ 'O', '~', '#' ]
NUMBER = [ '1', '2', '3' ]

class Deck
  attr_accessor :cards

  def initialize
    @cards = COLOR.product(TEXTURE)
    .product(FORM)
    .product(NUMBER)
    .map(&:flatten)
    .map { |details| Card.new details }
  end
end
