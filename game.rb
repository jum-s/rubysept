require_relative './lib/tapis'
require_relative './lib/deck'
require_relative './lib/attempt'

class Game
  attr_accessor :deck, :tapis

  def initialize
    @tapis = Tapis.new
    @deck = Deck.new
  end

  def start!
    @deck.cards.shuffle!
    @tapis.deal @deck
  end

  def next! set_cards
    remove_cards set_cards
    if @tapis.cards.size > 12
      @tapis.cards.compact!
    else
      insert_new_cards
    end
  end

  def get_hint
    set = first_set_found
    unless set
      if @tapis.cards.size > 12
        return "Oups, there is no set at all! Press (n) to start a new game."
      end
      return "There is no set here! Press (a) to add 3 cards."
    end
    set.map { |card| @tapis.letter card }.join
  end

  def try cards
    attempt = Attempt.new cards
    next!(cards) if attempt.is_set
    attempt
  end

  def add_cards
    if @tapis.cards.length < 15
      @tapis.add_cards(@deck)
    end
  end

  def first_set_found
    @tapis.cards.combination(3).to_a
    .find do |cards|
      Attempt.new(cards).is_set
    end
  end

  private

  def remove_cards set_cards
    @tapis.cards.map! do |card|
      set_cards.include?(card) ? nil : card
    end
  end

  def insert_new_cards
    new_cards = @deck.cards.shift(3)
    @tapis.cards.map! do |card|
      card or new_cards.shift
    end
  end
end
