# Play sept in the terminal

Open a terminal and clone the repository on your machine:

`git clone git@gitlab.com:jum-s/rubysept.git`

Then go in the created folder

`cd rubysept`

This little program is written in ruby. You can run it with the next command and enjoy your game:

`ruby run.py`

there is also a python version https://gitlab.com/jum-s/sept