require_relative '../game'

turn = 0
sets_found_counter = []
while turn < 40
  sets_found = 0
  @game = Game.new
  @game.start!
  while @game.get_hint != "Oups, there is no set at all! Press (n) to start a new game."
    while @game.first_set_found
      break if @game.deck.cards.length == 3
      cards = @game.first_set_found
      sets_found += 1
      @game.try(cards)
    end
    if @game.deck.cards.length == 3
      @game = Game.new
      @game.start!
    else
      @game.add_cards
    end
  end
  print "Turn #{turn}: #{sets_found} sets_found\n"
  turn += 1
  sets_found_counter << sets_found
end
print "Average : "
print sets_found_counter.sum / sets_found_counter.size
