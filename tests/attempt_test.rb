gem 'minitest', '>= 5.0.0'
require 'minitest/autorun'
require './lib/attempt'
require './lib/deck'
require './lib/card'

class AttemptTest < Minitest::Test
  def setup
    @deck = Deck.new
  end

  def test_valid_attempt
    cards = @deck.cards.first(3)
    attempt = Attempt.new cards
    assert_equal attempt.is_set, true
  end

  def test_invalid_attempt
    cards = @deck.cards[1..4]
    attempt = Attempt.new cards
    assert_equal attempt.is_set, false
  end

  def test_all_cards_different_attempt
    cards = [
      Card.new([31, "0", "1", "plain"]),
      Card.new([32, "~", "2", "line"]),
      Card.new([34, ".", "#", "empty"])
    ]
    attempt = Attempt.new cards
    assert_equal attempt.is_set, true
  end

  def test_card_display
    displays = @deck.cards.each do | card |
      lines = card.display
      assert_equal lines.count, 15
      lines.each do | line |
        assert line.chars.count >= 18
      end
    end
  end
end
