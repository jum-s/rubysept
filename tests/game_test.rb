gem 'minitest', '>= 5.0.0'
require 'minitest/autorun'
require_relative '../game'

class GameTest < Minitest::Test
  def setup
    @game = Game.new
    # game.deck is not shuffle to ease tests
    # so the first 3 cards are a valid set
    @game.tapis.deal @game.deck
  end

  def test_initial_game
    tapis_size = 12
    deck_size = 3**4 # 81

    assert_equal tapis_size, @game.tapis.cards.size
    assert_equal deck_size - tapis_size, @game.deck.cards.size
  end

  def test_next_deck
    first_set = @game.tapis.cards.first(3)
    @game.next! first_set
    refute_equal @game.tapis.cards.first(3), first_set
  end

  def test_add_three_cards_to_tapis
    @game.add_cards
    assert_equal @game.tapis.cards.length, 15
  end

  def test_cannot_add_six_cards_to_tapis
    @game.add_cards
    @game.add_cards
    assert_equal @game.tapis.cards.length, 15
  end

  def test_attempt_with_card_added
    @game.add_cards
    set = @game.tapis.cards[-3..-1]
    @game.next! set
    assert_equal @game.tapis.cards.length, 12
  end

  def test_get_hint
    assert_equal @game.get_hint, 'abc'
  end
end
