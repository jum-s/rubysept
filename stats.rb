require_relative '../game'

turn = 0
rounds_counter = []
while turn < 40
  rounds = 0
  @game = Game.new
  @game.start!
  while @game.get_hint != "Oups, there is no set at all! Press (n) to start a new game."
    while @game.first_set_found
      break if @game.deck.cards.length == 3
      cards = @game.first_set_found
      rounds += 1
      @game.try(cards)
    end
    if @game.deck.cards.length == 3
      @game = Game.new
      @game.start!
    else
      @game.add_cards
    end
  end
  print "Turn #{turn}: #{rounds} rounds\n"
  turn += 1
  rounds_counter << rounds
end
print "Average : "
print rounds_counter.sum / rounds_counter.size
