def start_game
  @game = Game.new
  @game.start!
  @game.tapis.display
end

def is_valid? player_attempt
  valid_size = player_attempt.size == 3
  valid_char = player_attempt.all? { |char| ('a'..'o').to_a.include?(char) }
  valid_char or valid_size
end

def sanitize input
  attempt = input.chars
  .map{ |l| to_number(l) }
  .keep_if do |input|
    (1..12).to_a.include? input
  end
end

def get_cards input
  attempt = input.map do |card_num|
    @game.tapis.cards[card_num - 1]
  end
  attempt if attempt.length == 3
end

def to_number letter
  number = ('a'..'o').to_a.index(letter)
  number + 1 if number.is_a? Integer
end

def play_attempt raw_player_attempt
  player_attempt = sanitize raw_player_attempt
  cards = get_cards player_attempt
  if cards
    attempt = @game.try cards
    @game.tapis.display
    attempt.resolve
  else
    print "Invalid input: '#{raw_player_attempt}', should be 3 letters between a & l (ex: 'abc' or 'gfe').\n"
  end
end
