require_relative 'game'
require_relative './lib/attempt'
require_relative 'run_helper'

INSTRUCTIONS = "Pick your set : (n) new game, (q) quit, (a) add 3 cards, (h) hint\n"

start_game
print INSTRUCTIONS

until @game.deck.cards.empty?
  raw_player_attempt = gets.chomp

  if raw_player_attempt == 'q'
    exit
  elsif raw_player_attempt == 'a'
    @game.add_cards
    @game.tapis.display
  elsif raw_player_attempt == 'n'
    start_game
  elsif raw_player_attempt == 'h'
    @game.tapis.display
    print "It's our little secret: #{@game.get_hint}\n"
  else
    print play_attempt raw_player_attempt
  end
  print INSTRUCTIONS
end
